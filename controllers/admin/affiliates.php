<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Affiliates extends Admin_Controller 
{
	protected $section = 'affiliates';
	private $data;	
	

	public function __construct() 
	{
		parent::__construct();

		$this->data = new StdClass();

		//check if has access
		role_or_die('shop', 'admin_affiliates');		
		
		// Load all the required classes
		$this->load->model('affiliates_m');

		Events::trigger('evt_admin_load_assests');	

	}


	/**
	 * List all items
	 * @access public
	 */
	public function index($offset= 0) 
	{
		$limit = 10;

		// Check for multi delete
		//if($this->input->post('action_to'))
		//{
		//	$this->delete(NULL);
		//}

		// Count all
		$total_rows = $this->affiliates_m->count_all();

		// Create Pagination
		$this->data->pagination = create_pagination('admin/shop/affiliates/index', $total_rows, $limit,5);

		// Otherwise display list
		$this->data->affiliates = $this->affiliates_m->limit($this->data->pagination['limit'], $this->data->pagination['offset'])->get_all();

		$this->template
				->title($this->module_details['name'])
				->build('admin/affiliates/list', $this->data);
	}

	public function create() 
	{

		if($input = $this->input->post())
		{
			$id = $this->affiliates_m->create($input);

			$this->data = $this->affiliates_m->get($id);
		}
		else
		{
			$this->data = new stdClass();
			$this->data->name = '';
		}

		$this->template
				->title($this->module_details['name'])
				->build('admin/affiliates/form', $this->data);
	}


	public function edit($id) 
	{

		if($input = $this->input->post())
		{
			$this->affiliates_m->edit($input);
		}

		$this->data = $this->affiliates_m->get($id);

		$this->template
				->title($this->module_details['name'])
				->build('admin/affiliates/form', $this->data);
	}

	public function delete($id)
	{
		if($this->affiliates_m->delete($id))
		{
			$this->session->set_flashdata('success','Affiliate deleted');
		}
		else
		{
			$this->session->set_flashdata('error','unable to delete');
		}

		redirect('admin/shop/affiliates');
	}

	public function link_to_product()
	{
		$input = $this->input->post();

		$ret_array = array();

		// We need to check the input first
		$products_id = $input['product_id'];
		$affiliate_id = $input['affiliate_id'];

		$medtod = $input['method'];
		$value = $input['value'];


		// Send the value
		if( $this->affiliates_m->link( $input ) )
		{
			$ret_array['status'] = JSONStatus::Success;
		}


		returnJSON($ret_array);
	}



	

}