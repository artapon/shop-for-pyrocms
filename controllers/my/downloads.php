<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Downloads extends Public_Controller 
{

	public function __construct() 
	{
   	
		parent::__construct();
		

		// If User Not logged in
		if (!$this->current_user) 
		{
			$this->session->set_flashdata('notice', lang('shop:my:user_not_authenticated'));
			
			// Send User to login then Redirect back after login
			$this->session->set_userdata('redirect_to', 'shop/my');
			redirect('users/login');
		}

		
		// Define the top level breadcrumb
		$this->template->set_breadcrumb(lang('shop:label:shop'), 'shop');

	}
	
	
	/**
	 * 
	 */
	public function index() 
	{
				
		//dont do anything here

	}
	
	
	

	/**
	 * Can only request 1 file at a time.
	 * Future enhancement could allow for a a single ZIP of all files
	 * @param  [type] $id       [description]
	 * @param  [type] $order_id [description]
	 * @param  string $pin      [description]
	 * @return [type]           [description]
	 */
	public function file($id, $order_id)
	{
		//pin is only req for guest customers, logged in customers will be valiated against logged in status
		$this->load->helper('download');
		$this->load->model('orders_m');
		$this->load->model('shop_files_m');
		//we must check that both the file and the pin are correct
		
		$order = $this->orders_m->get($order_id);


		if($order->pmt_status != 'paid')
		{

				$_message = 'Please pay for your order first';


				if($this->input->is_ajax_request())
				{
					die(json_encode(
							array(
									'status' => 'error', 
									'message'=>$_message,
									'dlcount'=>0
								)
							)
					);
				}
				else
				{
					$this->session->set_flashdata("error",$_message);

					redirect("shop/my/orders/order/".$order_id);
				}


		}


		$fileObject = $this->shop_files_m->do_download($id, $order_id);


		if(!$fileObject->pass)
		{

				if($this->input->is_ajax_request())
				{
					die(json_encode(
							array(
									'status' => 'error', 
									'message'=> $fileObject->message,
									'dlcount'=>$fileObject->download_count
								)
							)
					);
				}
				else
				{
					$this->session->set_flashdata("error",$fileObject->message);

					redirect("shop/my/orders/order/".$order_id);
				}


		}

		//Get contents of file
		//$this->config->load('files');
		$this->load->library('files/files');
		$bar = Files::get_file($fileObject->file->data);

		$_filename = $bar['data']->filename;


		$mypath = FCPATH . $this->config->item('files:path') .  $_filename;


		$contents =  file_get_contents( $mypath);


		force_download($fileObject->file->filename, $contents); 

	}
	 
	
}