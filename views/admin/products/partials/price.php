

			<fieldset>

				<label>
					
					<?php echo lang('shop:products:price_tab_description'); ?>

					<small><?php echo lang('shop:products:price_tab_description_description'); ?></small>

				</label>



				<div class="tabs">		

					<ul class="tab-menu">
						<li><a class=""  data-load="" href="#price-basic-tab"><span><?php echo lang('shop:products:standard'); ?></span></a></li>		
						<li><a class=""  data-load="" href="#price-qty-tab"><span><?php echo lang('shop:products:qty_discount'); ?></span></a></li>	
						<li><a class=""  data-load="" href="#price-affiliate-tab"><span><?php echo lang('shop:common:affiliates'); ?></span></a></li>																	
					</ul>	


					<div class="form_inputs" id="price-basic-tab">
						<fieldset>
								<ul>
									<li>
										<label for="price"><?php echo lang('shop:common:price'); ?><span>*</span><br />
											<small><?php echo lang('shop:products:price_description'); ?></small>
										</label>
										<div class="input">
											<?php echo ss_currency_symbol().' '.form_input('price', set_value('price', $price), "placeholder='0.00'"); ?>
										</div>
									</li>

									<li>
										<label for="price_base"><?php echo lang('shop:products:base_price'); ?> <span></span><br />
											<small><?php echo lang('shop:products:base_price_description'); ?></small>
										</label>
										<div class="input">
											<?php echo ss_currency_symbol().' '.form_input('price_base', set_value('price_base', $price_base), "placeholder='0.00'"); ?>
										</div>
									</li>					
									<li>
										<label for="price"><?php echo lang('shop:products:rrp'); ?> <span></span><br />
											<small><?php echo lang('shop:products:rrp_description'); ?></small>
										</label>
										<div class="input">
											<?php echo ss_currency_symbol().' '.form_input('rrp', set_value('rrp', $rrp), "placeholder='0.00'"); ?>
										</div>
									</li>					
								</ul>
						</fieldset>
					</div>



					<div class="form_inputs" id="price-qty-tab">
						<fieldset>
							<ul>
								<li class="<?php echo alternator('', 'even'); ?>">
									<label for="pgroup_id">
											<?php echo lang('shop:products:pgroup'); ?>
										<small>
										<?php echo lang('shop:products:pgroup_description'); ?>
										</small>						
									</label>
									<div class="input">
										
										<?php echo $group_select; ?> 
										
									</div>
								</li>		
							</ul>
						</fieldset>
					</div>

					<div class="form_inputs" id="price-affiliate-tab">
						<fieldset>
							<ul>
								<li class="<?php echo alternator('', 'even'); ?>">

									<?php echo $affiliate_select; ?> 

									<select name='aff_method'>
										<option value='percent'>Percentage</option>
										<option value='fixed'>Fixed Cost</option>
									</select>

									<input type='text' name='aff_value' placeholder='0.00'>

									<input type='text' name='aff_min_qty' placeholder='5'>

									<a href='#' class='add_affiliate'><?php echo lang('shop:common:add');?></a>
									
								</li>		
							</ul>
						</fieldset>
					</div>

				</div>


			</fieldset>			
	


		<script>

				// 
				// Remove image from gallery
		        //
				$(".add_affiliate").live('click', function(e)  {					
					
					var aff_id = $("[name='affiliate_id']").val();
					var pid = $("#static_product_id").attr('data-pid');     /*get the product id*/
					var val = $("[name='aff_value']").val();     /*get the product id*/
					var min = $("[name='aff_min_qty']").val();     /*get the product id*/
					var mthd = $("[name='aff_method']").val();     /*get the product id*/
						            
					var send_data = 	{	affiliate_id:aff_id,	product_id:pid,		value:val,	method:mthd,   min_qty:min} ;

		            $.post('shop/admin/affiliates/link_to_product', send_data ).done(function(data) 
		            {			
		                var obj = jQuery.parseJSON(data);
		                
		                if (obj.status == 'success') 
		                {
		                   	alert('success');
		                }
		                else
		                {
		                	alert('fail');
		                }

		            });
		            
					return false;
				
				});	

		</script>