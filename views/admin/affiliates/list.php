<section class="title">
	<h4><?php echo lang('shop:common:affiliates'); ?></h4>
	<h4 style="float:right">
	<a title='<?php echo lang('shop:common:new'); ?>' href="admin/shop/affiliates/create" class='tooltip-s img_icon_title img_create'></a>
	</h4>
</section>
<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<section class="item">
	<div class="content">
		<?php if (empty($affiliates)): ?>
			<div class="no_data">
				<p>
					<?php echo lang('shop:common:description'); ?>
				</p>
				<?php echo lang('shop:common:no_data'); ?>
			</div>
		</section>
	<?php else: ?>
		<table>			
			<thead>
				<tr>
					<th><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
					<th></th>
					<th><?php echo lang('shop:common:name'); ?></th>
					<th style="width: 120px"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($affiliates AS $item): ?>
					<tr class="<?php echo alternator('even', ''); ?>">
						<td><input type="checkbox" name="action_to[]" value="<?php echo $item->id; ?>"  /></td>
						<td>

							<div class="img_48 img_groups"></div>
							
						</td>							
						<td><?php echo $item->name; ?></td>
						<td>

							<span style="float:right;">

								<span class="button-dropdown" data-buttons="dropdown">
								
										<a href="#" class="shopbutton button-rounded button-flat-primary"> 
											<?php echo lang('shop:common:actions');?> 
											<i class="icon-caret-down"></i>
										</a>
										 
										<!-- Dropdown Below Button -->
										<ul class="button-dropdown-menu">

											<li class=''><a title='<?php echo lang('shop:common:edit'); ?>' class="" href="<?php echo site_url('admin/shop/affiliates/edit/' . $item->id); ?>"> <?php echo lang('shop:common:edit');?>  </a></li>
											<li class=''><a title='<?php echo lang('shop:common:copy'); ?>' class="" href="<?php echo site_url('admin/shop/affiliates/duplicate/' . $item->id); ?>"> <?php echo lang('shop:common:copy');?> </a></li>
											<li class='button-dropdown-divider delete'><a title='<?php echo lang('shop:common:are_you_sure'); ?>' class="tooltip-e confirm" href="<?php echo site_url('admin/shop/affiliates/delete/' . $item->id); ?>"><?php echo lang('shop:common:delete');?>  </a></li>

										</ul>

								</span>

							</span>



						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="7">
					<div style="">
						<?php echo $pagination['links'];?>
						
					</div></td>
				</tr>
			</tfoot>
		</table>

		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
	</div>
	</section>
<?php endif; ?>

<?php echo form_close(); ?>

